package com.realdolmen.services;

import com.realdolmen.domain.Soldier;
import com.realdolmen.repository.BarrackRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class SoldierServiceImpl implements SoldierCommands{

    private BarrackRepository barrackRepository = new BarrackRepository();

    @Override
    public void sendToBarracks(Soldier soldier) {
        log.debug("A soldier is being added to barrack");
//        try { //Ter demonstratie
//            throw new Exception("ERROR!");
//        }catch (Exception e){
//            log.error("Something went wrong", e);
//        }
        barrackRepository.save(soldier);
    }

    @Override
    public List<Soldier> findAll() {
        return barrackRepository.findAllFromDb();
    }
}
