package com.realdolmen.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder(builderMethodName = "humanBuilder")
public class Human {
    private int legs;
    private int arms;
    private String name;
    private int head;
    private int id;

    public static HumanBuilder builder() {
        return humanBuilder().legs(2).head(1).arms(2);
    }


}
