package com.realdolmen.repository;

import com.realdolmen.domain.Soldier;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BarrackRepositoryTest {

    private BarrackRepository barrackRepository = new BarrackRepository();

    private Soldier soldier;

    @BeforeEach
    public void setup() {
        this.soldier = Soldier.builder("Test name").build();
    }

    @AfterEach
    public void cleanUpDataBase() {
        if (soldier != null) {
            barrackRepository.deleteById(soldier.getId());
        }
    }

    @Test
    public void save() {
        barrackRepository.save(this.soldier);
        Soldier soldierFromDb = barrackRepository.findById(soldier.getId());
        assertEquals("Test name", soldierFromDb.getName());
    }

    @Test
    public void findAllTest() {
        barrackRepository.save(this.soldier);
        List<Soldier> soldierList = barrackRepository.findAllFromDb();
        Assertions.assertFalse(soldierList.isEmpty());
        assertAll(assertName("Tom", soldierList.get(0))
                , assertName("Tim", soldierList.get(1)));
    }

    private Executable assertName(String name, Soldier actualSoldier) {
        return () -> assertEquals(name, actualSoldier.getName());
    }
}
